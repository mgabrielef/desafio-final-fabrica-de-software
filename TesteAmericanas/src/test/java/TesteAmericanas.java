import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TesteAmericanas {

    private WebDriver driver;

    @Before
    public void abrirNavegador(){
        //Local do geckodriver
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\maria\\Downloads\\geckodriver\\geckodriver.exe");
        // Instanciar FirefoxDriver
        driver = new FirefoxDriver();
        // Abrir janela do navegador
        driver.manage().window().maximize();
        //Inserir no navegador instrução para ir para o site das Americanas
        driver.get("https://www.americanas.com.br/");
    }

    @After
    public void fecharNavegador(){
        driver.quit();
    }

    @Test
    public void testesAmericanas(){
        //Buscar produto e enviar pesquisa
        driver.findElement(By.xpath("/html/body/div[1]/div/div/header/div[1]/div[1]/div/div[1]/form/input")).sendKeys("The last of us");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/header/div[1]/div[1]/div/div[1]/form/button")).click();

        //Verificar se busca foi realizada corretamente
        Assert.assertEquals("The Last Of Us: Promoções e Ofertas na Americanas", driver.getTitle());

        //Selecionar produto da lista
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/div/div[3]/div[2]/div[1]/div/div/a/div[2]/div[2]/h3")).click();

        //Adicionar ao carrinho
        driver.findElement(By.xpath("/html/body/div[1]/div/div/main/div[3]/div[2]/div[3]/a")).click();

        //Verificar se produto foi adicionado ao carrinho
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[1]/div/section/div/article/div[1]/a/h3")).getText().contains("Game The Last Of Us Part I - PS5"));
    }

}
